import { NextApiRequest, NextApiResponse } from 'next';
import formidable from 'formidable';
import fs from 'fs';
import path from 'path';
import { v4 as uuidv4 } from 'uuid';
import db from '../../models';

const Geodata = db.geodata;

export default async function POST(req: NextApiRequest, res: NextApiResponse) {
    const form = new formidable.IncomingForm();
    form.uploadDir = './uploads'; // Specify the upload directory
    form.keepExtensions = true; // Keep file extensions
    form.parse(req, async (err, fields, files) => {
        if (err) {
            return res.status(500).json({ message: "Failed to parse the form data." });
        }
        const file = files.uploads; // Assuming 'uploads' is the name of the file input field

        // Check if file exists
        if (!file || !file.name) {
            return res.status(400).json({ message: "No file uploaded." });
        }

        // Rename the file to avoid conflicts and ensure uniqueness
        const newFileName = uuidv4() + path.extname(file.name);
        const newPath = path.join(form.uploadDir, newFileName);

        try {
            fs.renameSync(file.path, newPath);

            // Read the content of the JSON file
            const jsonContent = fs.readFileSync(newPath, 'utf8');

            // Do something with the JSON content (e.g., save to database)
            // For example:
            // await Geodata.create({ content: jsonContent });
            jsonContent.forEach((data:any) => {
                Geodata.create(data);
            });
            

            return res.status(200).json({ message: "File uploaded successfully." });
        } catch (error) {
            return res.status(500).json({ message: "Error while processing the uploaded file." });
        }
    });
}
