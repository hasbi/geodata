'use client';
import type { NextApiRequest, NextApiResponse } from 'next';
const db = require('../../models');
 
const Geodata = db.geodata
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const data = req.body
  const id = await Geodata.create(data);
  res.status(200).json({ id })
}