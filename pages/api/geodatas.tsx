import { NextApiRequest, NextApiResponse } from 'next';
// import db from "../../../models";
const db = require('../../models')

const Geodata = db.geodata

export default async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === 'GET') {
    try {
      const geodatas = await Geodata.findAll();
      console.table(geodatas);
      res.status(200).json(geodatas);
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
      console.log(error);
    }
  } else if (req.method === 'POST') {
    const { name, lat, lon } = req.body;
    try {
      const geodata = await Geodata.create({ name, lat, lon });
      res.status(201).json(geodata);
      console.table(geodata);
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
      console.log(error);
    }
  } else {
    res.status(405).json({ error: 'Method Not Allowed' });
  }
};