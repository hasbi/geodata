import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
    './models/**/*.{js,ts,jsx,tsx,mdx}',
    './reotes/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    colors:{
      "primary-purple-light" : "#95778d",
      "primary-purple-dark" : "#564353",
      "primary-white" : "#ffffff"
    },
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
          "banner" : "url('../img/nature.jpg')",
          "about" : "url('../img/forest.jpg')",
          "contactus" : "url('../img/landmark.jpg')",
      },
    },
  },
  plugins: [],
}
export default config
