const parseJSON = require('./jsonParser');
const fs = require('fs');


test('parseJSON should return an object with correct values', () => {
    fs.readFile('/uploads/geodata.json', 'utf8', (err, data) => {
        if (err) {
          console.error(err);
          return;
        }
        data.forEach(key => {
            const parsedObject = parseJSON(key);

            expect(parsedObject).toBeDefined(); // Check if the parsed object is defined
            expect(parsedObject).toEqual({ name: 'Bogor', lat: "-8.23434235",lon:"107.923434"}); // Check if the parsed object matches the expected object
        });        
      });  
});