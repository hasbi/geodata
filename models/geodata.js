module.exports = (sequelize, Sequelize) => {
    const Geodata = sequelize.define("geodata", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },  
        name: {
            type: Sequelize.STRING
        },
        lat: {
            type: Sequelize.STRING
        },
        lon: {
            type: Sequelize.STRING
        }
    });
  
    return Geodata;
};