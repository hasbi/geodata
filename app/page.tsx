// import Image from 'next/image';
import Head from "next/head";
import { Banner } from "../components/banner";
import { About } from "../components/about";

export default function Home() {
  return (
    <>
      <Head>
          <title>Geo Data</title>
      </Head>
      <div>
        <About />
        <Banner />
      </div>
    </>
  );
}
