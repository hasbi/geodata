'use client';
import React, { useEffect, useState }  from "react";
import axios from 'axios';
// import { Navbar } from "./navbar";
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';

export const Banner = () => {
    const [geodatas, setGeodatas] = useState([]);

    useEffect(() => {
        async function fetchGeodatas() {
            try {
                const response = await axios.get('/api/geodatas');
                console.log('data anjing');
                console.table(response);
                setGeodatas(response.data);
            } catch (error) {
                console.error(error);
            }
        }

        fetchGeodatas();
    },[]);

    return (
        <>
            {/* background */}
            <div className="h-screen w-full bg-banner bg-cover">
                {/* container */}
                <div className="w-[80%] mx-auto bg-primary-purple-light flex flex-col justify-center items-center p-8 md:p-8 lg:p-16 xl:p-24 gap-12">
                    <MapContainer center={[-2.2227826,95.8725871]} zoom={5} style={{ height: '500px', width: '100%' }}>
                        <TileLayer
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        />

                            {
                                geodatas.length > 0 ? (
                                    geodatas.map((geodata) => (
                                        <Marker position={[geodata.lat,geodata.lon]}>
                                            <Popup>
                                                {geodata.name}
                                            </Popup>
                                        </Marker>
                                    ))
                                ) :(
                                    <Marker position={[-2.2227826,95.8725871]}>
                                        <Popup>
                                            Indonesia
                                        </Popup>
                                    </Marker>
                                )
                            }
                        
                    </MapContainer>
                    
                </div>
            </div>
        </>
    );
};