import React from "react";
import { Button } from "./button";

export const Contactus = () => {
    return (
        <>
            {/* background */}
            <div className="h-screen w-full bg-contactus bg-cover flex justify-center items-center">
                <div className="m-12 w-[80%] mx-auto bg-primary-purple-light flex flex-col justify-center items-center p-8 md:p-16 lg:p-24 xl:p-32 gap-24">
                    <h2 className="text-primary-white">Contact Us</h2>
                    <div className="w-full flex flex-col lg:flex-row xl:flex-row justify-center items-center lg:items-end xl:items-end gap-12">
                        <div className="flex flex-col items-start">
                            <p className="text-primary-white text-left w-2/3">alihasbi001@gmail.com</p>
                            <p className="text-primary-white text-left w-2/3">081290878010</p>
                            <p className="text-primary-white text-left w-2/3">Deliserdang, Medan</p>
                            <p className="text-primary-white text-left w-2/3">0344 3434322342</p>
                        </div>
                    </div>
                    <Button text="Reserve your slot" />
                </div>
            </div>
        </>
    );
};