"use client";
import React, { useEffect, useState, FormEvent }  from "react";
import axios from 'axios';
import FileUploader from "./FileUpload";


export const About = () => {
    const [geodatas, setGeodatas] = useState([]);
    const url = '/api/fileupload';
    useEffect(() => {
        async function fetchGeodatas() {
            try {
                const response = await axios.get('/api/geodatas');
                console.log('data anjing');
                console.table(response);
                setGeodatas(response.data);
            } catch (error) {
                console.error(error);
            }
        }

        fetchGeodatas();
    },[]);

    async function onSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault()
     
        const formData = new FormData(event.currentTarget)
        const response = await fetch('/api/upload', {
          method: 'POST',
          body: formData,
        })
     
        // Handle response if necessary
        const data = await response.json()
        // ...
      }

    return (
        <>
            {/* background - flex box*/}
            <div className="h-fit w-full bg-about bg-cover flex justify-center items-center">
                {/* content - purple flex box*/}
                <div className="m-12 w-[80%] mx-auto bg-primary-purple-light flex flex-col justify-center items-center p-8 md:p-16 lg:p-24 xl:p-32 gap-12">
                    <FileUploader
                        url={url}
                        acceptedFileTypes={[
                        "application/json"
                        ]}
                        maxFileSize={1}
                        label="Max File Size: 1MB"
                        labelAlt="Accepted File Types: json"
                    />
                    <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                        <thead className="bg-white text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <td className="px-6 py-3">Location</td>
                                <td className="px-6 py-3">Latittude</td>
                                <td className="px-6 py-3">Longitude</td>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                geodatas.length > 0 ? (
                                    geodatas.map((geodata) => (
                                        <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"  key={geodata.id}>
                                            <td className="px-6 py-4">{geodata.name}</td>
                                            <td className="px-6 py-4">{geodata.lat}</td>
                                            <td className="px-6 py-4">{geodata.lon}</td>
                                        </tr>
                                    ))
                                ) :(
                                    <tr>
                                        <td colSpan={4}>memek</td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </>
    );
};
