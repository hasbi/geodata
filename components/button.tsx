import React from "react";

type Props = {
    text?: string,
    className?: string
};

export const Button = ({text, className}: Props) => {
    return (
        <div className={`${className} px-4 py-2 border-2 border-primary-white text-primary-white`} >
            {text}
        </div>
    );
};