'use client';
import React from "react";
import Image from "next/image";
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';

export const Experience = () => {
    return (
        <>
            {/* background - flex box*/}
            <div className="py-12 h-fit w-full bg-primary-purple-light flex flex-col md:flex-row lg:flex-row xl:flex-row justify-center items-start gap-12 md:gap-0 lg:gap-0 xl:gap-0">
                {/* left - flex box*/}
                <div className="md:w-3/5 lg:w-3/5 xl:w-3/5 flex flex-col gap-8">
                    {/* title */}
                    <h1 className="text-primary-white flex-l">Portofolio</h1>
                    {/* image and title - flex box*/}
                    <div className="px-12 md:pr-6 lg:pr-6 xl:pr-6 flex flex-col justify-center items-end gap-4">
                        {/* Image */}
                        <Image className="w-screen" src="/img/nextjs.jpg" alt="next js" width={500} height={500} />
                        
                        {/* Subtitle - flex box */}
                        <div className="flex flex-col items-end">
                            <p className="text-primaryu-white font-bold">
                                Next Js
                            </p>
                            <p className="text-primaryu-white">
                                A shot of the family
                            </p>
                        </div>
                    </div>
                </div>
                <div className="md:w-2/5 lg:w-2/5 xl:w-2/5 flex flex-col gap-8">
                    {/* Image */}
                    <Image className="w-screen h-auto" src="/img/nextjs2.jpg" alt="next js 2" width={500} height={500} />
                        {/* Subtitle - flex box */}
                        <div className="flex flex-col items-end">
                            <p className="text-primaryu-white font-bold">
                                Next Js 2
                            </p>
                            <p className="text-primaryu-white">
                                A shot of the family
                            </p>
                        </div>
                </div>
            </div>
        </>
    );
};