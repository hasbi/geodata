export const Navbar = () => {
    return (
        <div className="flex justify-start">
            <h3 className="text-primary-purple">Geo Data</h3>
        </div>
    );
};